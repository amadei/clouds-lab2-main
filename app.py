from math import sin

from flask import Flask, request, jsonify, Response

app = Flask(__name__, static_folder='./static/')


PARTITIONS = [10, 100, 1_000, 10_000, 100_000, 1_000_000]
USE_CACHE = False


cache: dict[tuple[float, float], dict[int, float]] = {}


def compute_integral(lower: float, upper: float, n: int = 1000) -> float:
    def f(x): return abs(sin(x))

    integral = 0
    delta = (upper - lower) / n
    a = lower
    b = lower + delta
    for i in range(n):
        integral += 0.5 * (b - a) * (f(a) + f(b))
        a = b
        b = a + delta

    return integral


def get_error(message: str, code: int) -> tuple[Response, int]:
    return jsonify({'message': message, 'code': code}), code


def get_result(lower, upper) -> dict[int, float]:
    return {i: compute_integral(lower, upper, n=i) for i in PARTITIONS}


@app.route('/')
def index():
    return app.send_static_file('index.html')


@app.route('/api/numerical_integration', methods=['GET'])
def numerical_integration():
    lower = request.args.get("lower", type=float)
    upper = request.args.get("upper", type=float)

    if lower is None or upper is None:
        return get_error("'lower' and 'upper' parameters are required amd must be float", 400)

    if lower > upper:
        return get_error("'lower' parameter must be lower than 'upper' parameter", 400)

    if not USE_CACHE:
        return jsonify(get_result(lower, upper)), 200

    if (lower, upper) not in cache:
        cache[(lower, upper)] = get_result(lower, upper)

    return jsonify(cache[(lower, upper)]), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
