from locust import HttpUser, task


class IntegrationUser(HttpUser):
    @task
    def hello_world(self):
        lower = 0
        upper = 3.14159

        self.client.get(f"/api/numerical_integration?lower={lower}&upper={upper}")
