

TITLE_LINE = 'Type     Name                                                                          # reqs      # fails |    Avg     Min     Max    Med |   req/s  failures/s'
TABLE_LINE = '--------|----------------------------------------------------------------------------|-------|-------------|-------|-------|-------|-------|--------|-----------'

FILES = ['test_local.txt', 'test_vms.txt', 'test_app.txt', 'test_func.txt']


def parse_test_file(name: str) -> list[tuple[int, float]]:
    with open(name, 'r') as file:
        lines = [line.rstrip() for line in file]

        in_table = False
        values = []
        t = 2
        for line in lines:
            if line == TITLE_LINE or line == '':
                continue

            if line == TABLE_LINE:
                in_table = not in_table
                continue

            if not in_table:
                values.append((t, float(line.split()[-2])))
                t += 2

        return values


def write_csv(path: str, values: list[list[tuple[int, float]]]) -> None:
    with open(path, 'w') as file:

        matrix = [[str((i + 1) * 2)] for i in range(len(values[0]))]

        for i in range(len(values[0])):
            for j in range(len(FILES)):
                matrix[i].append(str(values[j][i][1]))

        columns = ['t'] + [i.rsplit('.')[0] for i in FILES]
        file.write(';'.join(columns) + '\n')

        for i in range(len(values[0])):
            file.write(';'.join(matrix[i]) + '\n')


if __name__ == '__main__':
    write_csv('tests.csv', [parse_test_file(i) for i in FILES])
