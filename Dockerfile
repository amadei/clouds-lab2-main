FROM python:3.9.18-alpine3.19

EXPOSE 80
WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY app.py .
COPY wsgi.py .
COPY static ./static

ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:80", "wsgi:app"]
# ENTRYPOINT ["flask", "--app", "wsgi", "run", "--host=0.0.0.0", "--port=5000"]
